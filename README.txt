README.txt
==========

This is a module that adjusts bxSlider(http://bxslider.com/) jQuery slideshow
functionality into Drupal according to Drupal idioms.

 It also offers
 - Registers bxSlider library.
 - Defines theme functions. That should not be used. Because they are intermediates.
 - Prepares renderable array to be rendered.
 - Defines the bxSlider formatter for image fields.
 - Defines the bxSlider views style plugin.

INSTALLATION
============
 - Install module in a drupal way modulu installation.
 - Upload bxslider JS library in the folder /sites/all/libraries

USAGE
=====
 - You should use in a drupal way: field formatters, view style plugins.

AUTHOR/MAINTAINER
=================
- Viktor Likin <viktor.likin at gmail DOT com> http://www.shelepen.com.ua