<?php
/**
 * @file Contains helpers.
 */

function _bxslier_settings_form($values) {
  $form = array();
  $image_styles = image_style_options(FALSE);
  $form['imageStyle'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#default_value' => $values['imageStyle'],
    '#empty_option' => t('None (original image)'),
    '#options' => $image_styles,
  );
  $form['slideWidth'] = array(
    '#title' => t('Slide width'),
    '#type' => 'number',
    '#default_value' => $values['slideWidth'] ? $values['slideWidth'] : 0,
    '#min' => 0,
    '#required' => TRUE,
  );
  $form['minSlides'] = array(
    '#title' => t('Min slides'),
    '#type' => 'number',
    '#default_value' => $values['minSlides'] ? $values['minSlides'] : 1,
    '#min' => 1,
    '#required' => TRUE,
  );
  $form['maxSlides'] = array(
    '#title' => t('Max slides'),
    '#type' => 'number',
    '#default_value' => $values['maxSlides'] ? $values['maxSlides'] : 1,
    '#min' => 1,
    '#required' => TRUE,
  );
  $form['slideMargin'] = array(
    '#title' => t('Slide margin'),
    '#type' => 'number',
    '#default_value' => $values['slideMargin'] ? $values['slideMargin'] : 0,
    '#min' => 0,
    '#required' => TRUE,
  );
  $form['pager'] = array(
    '#title' => t('Show pager'),
    '#type' => 'checkbox',
    '#default_value' => $values['pager'] ? $values['pager'] : TRUE,
  );

  return $form;
}

/**
 * Prepares a renderable array ro theme bxslider.
 */
function _bxslider_get_render_source($items, $settings, $ukey='ukey') {
  $source = array (
    '#theme' => 'bxslider_wrapper',
    'items' => $items,
    '#ukey' => $ukey,
    '#attached' => array (
      'library' => array (
        array('bxslider', 'bxslider'),
      ),
      'js' => array (
        array (
          'data' => drupal_get_path('module', 'bxslider') . '/js/script.js',
          'type' => 'file',
        ),
        array (
          'data' => array (
            'bxslider' => array (
              'formatter' => array (
                $ukey => $settings,
              ),
            ),
          ),
          'type' => 'setting',
        ),
      ),
    ),
  );
  
  return $source;
}