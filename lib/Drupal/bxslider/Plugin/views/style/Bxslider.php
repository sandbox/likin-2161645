<?php

/**
 * @file
 * Definition of Drupal\bxslider\Plugin\views\style\Bxslider.
 */

namespace Drupal\bxslider\Plugin\views\style;

use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Style plugin to render each item in bxslider.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "bxslider",
 *   title = @Translation("BxSlider"),
 *   help = @Translation("It wrappes rows into a slider."),
 *   theme = "bxslider_view_bxslider",
 *   display_types = {"normal"}
 * )
 */
class Bxslider extends StylePluginBase {
  /**
   * Overrides \Drupal\views\Plugin\views\style\StylePluginBase::$usesRowPlugin.
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Render the given style.
   */
  public function buildOptionsForm(&$form, &$form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form += _bxslier_settings_form($this->options);
  }

  /**
   * Overrides Drupal\views\Plugin\views\style\StylePluginBase::render()
   */
  public function render() {
    $elements = array();
    foreach ($this->view->result as $row) {
      $elements[] = $this->view->rowPlugin->render($row);
    }
    $ukey = $this->view->storage->id() . '__' . $this->view->current_display;
    return _bxslider_get_render_source($elements, $this->options, $ukey);
  }
}
