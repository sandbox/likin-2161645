<?php
/**
 * @file bxSlider Formatter. Contains Drupal\bxslider\Plugin\Field\FieldFormatter.
 */

namespace Drupal\bxslider\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'bxslider' formatter.
 *
 * @FieldFormatter(
 *   id = "bxslider",
 *   label = @Translation("bxSlider"),
 *   field_types = {
 *     "image"
 *   },
 *   settings = {
 *     "image_style" = "thumbnail",
 *     "slideWidth" = 200,
 *     "minSlides" = 2,
 *     "maxSlides" = 4,
 *     "slideMargin" = 0,
 *     "pager" = 1
 *   }
 * )
 */
class BxsliderFormatter extends FormatterBase { 
  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, array &$form_state) {
    $element = _bxslier_settings_form($this->getSettings());

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $ukey = substr(sha1(time() . 'bxslider'), 0, 5);
    $elements = array();
    foreach ($items as $delta => $item) {
      if ($item->entity) {
        $elements[$delta] = array(
          '#theme' => 'image_formatter',
          '#item' => $item->getValue(TRUE),
          '#image_style' => $this->getSetting('image_style'),
        );
      }
    }
    $source = _bxslider_get_render_source($elements, $this->getSettings(), $ukey);

    return array($source);
  }
}
